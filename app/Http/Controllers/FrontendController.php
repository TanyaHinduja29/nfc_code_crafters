<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Scholarship;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }
    public function scholarship()
    {

        if (request()->has('search')) {

            $json = $this->filterScholarShip();
            return $json;
        }
        $scholarships = Scholarship::get();
        return view('frontend.scholarship', compact([
            'scholarships'
        ]));
    }
    public function filterScholarShip()
    {
        $searchParam = request()->search;
        //dd($searchParam);

        $scholarships = Scholarship::where('name', 'like', "%$searchParam%")->get();

        return response()->json([
            'scholarShips' => $scholarships,

        ], 200);
    }
}
