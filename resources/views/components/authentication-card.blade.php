<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-red-100 bg-img"
    style="background-image: url('{{ asset('assets/images/login_reg/lr.jpg') }}')">
    {{-- <div>
        {{ $logo }}
    </div> --}}

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>

<style>
    .bg-img {
        background-color: rgba(0, 0, 0);
        background-size: cover;
        opacity: 0.6;
    }
</style>
