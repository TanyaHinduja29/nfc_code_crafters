@extends('frontend.layouts.app')
@section('content')
    <main id="main" data-aos="fade-in">

        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="container">
                <h2>Scholarships</h2>
                <p>Est dolorum ut non facere possimus quibusdam eligendi voluptatem. Quia id aut similique quia voluptas sit
                    quaerat debitis. Rerum omnis ipsam aperiam consequatur laboriosam nemo harum praesentium. </p>
                <br>
                <form action="" method="post" class="search-form"
                    onsubmit="event.preventDefault(); searchScholarships(this.search.value);" id="searchForm">>
                    <input class="search-bar" type="text" name="search" id="searchInput"><input type="submit"
                        value="Search">
                    {{-- <button type="submit" name="searchSubmit"><i class="bi bi-search"></i></button> --}}
                </form>



            </div>
        </div><!-- End Breadcrumbs -->

        <!-- ======= Courses Section ======= -->
        <section id="courses" class="courses">
            <div class="container" data-aos="fade-up">

                <div class="row gx-5" data-aos="zoom-in" data-aos-delay="100" id="scholarships">
                    @if ($scholarships)
                        @foreach ($scholarships as $scholarship)
                            <div class="card mb-3 col-12 col-md-4 border-0" style="">
                                <div class="row no-gutters">
                                    <div class="col">
                                        <div class="card-body custom-border">
                                            <h5 class="card-title">{{ $scholarship->name }}</h5>
                                            <p class="card-text">{{ Str::limit($scholarship->description, 100) }}.</p>
                                            <p class="card-text"><small class="text-muted">Deadline:
                                                    {{ $scholarship->deadline }}</small></p>
                                            <div class="text-center">
                                                <a href="courses.html" class="more-btn popular-scholarship-btn">Apply
                                                    now</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif



                </div>

            </div>
        </section><!-- End Courses Section -->

    </main><!-- End #main -->
@endsection

@section('scripts')
    <script>
        //console.log("here")

        function searchScholarships(searchParam) {
            console.log("SerahParam:- " + searchParam);
            let route = `http://localhost:8000/scholarships/?search=${searchParam}`;
            const xhr = new XMLHttpRequest();
            xhr.open("GET", route, true);
            xhr.send();

            let scholarships = document.getElementById('scholarships');
            //console.log(xhr.readyState);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    let response = JSON.parse(xhr.responseText);
                    console.log(response.scholarShips);
                    let length = response.scholarShips.length;
                    let $scholarship = '';
                    for (let i = 0; i < length; i++) {
                        $scholarship += `<div class="card mb-3 col-12 col-md-4 border-0" style="">
                        <div class="row no-gutters">
                            <div class="col">
                                <div class="card-body custom-border">
                                    <h5 class="card-title">${response.scholarShips[i].name}</h5>
                                    <p class="card-text">${response.scholarShips[i].description}<.</p>
                                    <p class="card-text"><small class="text-muted">Deadline:
                                            ${response.scholarShips[i].deadline}</small></p>
                                    <div class="text-center">
                                        <a href="courses.html" class="more-btn popular-scholarship-btn">Apply now<i
                                                class="bx bx-chevron-right"></i></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>`

                    }

                    console.log($scholarship);
                    scholarships.innerHTML = $scholarship;


                }
            }
        }
    </script>
@endsection
