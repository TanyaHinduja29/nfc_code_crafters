<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Scholarship>
 */
class ScholarshipFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $owner = ['college', 'government'];
        return [
            'name'=>fake()->word(),
            'description'=>fake()->paragraphs(1,true),
            'department_name'=>fake()->word(),
            'owner'=>$owner[array_rand($owner)],
            'deadline'=>Carbon::now()->format('Y-m-d H:i:s')
        ];
    }
}
